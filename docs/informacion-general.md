---
title: Información general
---

- [Programación de la asignatura](assets/INF-2DAW-DWESE-C19-20.pdf){:target="_blank"}
- **Normativa de aplicación**: [Orden de 16 de junio de
  2011](http://www.juntadeandalucia.es/boja/2011/149/23), por la que se
  desarrolla el currículo correspondiente al título de Técnico Superior en
  Desarrollo de Aplicaciones Web.
- **Equivalencia en créditos ECTS**: 12.
- **Código**: 0613.
- **Duración total**: 168 + 42 horas / 21 semanas
- **Carga lectiva semanal**: 8 horas + 2 horas libre configuración
