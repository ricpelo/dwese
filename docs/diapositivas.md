---
title: Diapositivas y apuntes
---

Los materiales de las diferentes unidades didácticas se organizan de la siguiente manera:

- **HTML**: Diapositivas para su visualización *online* desde un navegador, por lo que requiere conexión a Internet. Estructura las diapositivas en una matriz de filas y columnas para facilitar la navegación. Además pueden encontrarse actividades de preguntas y respuestas interactivas sencillas intercalados en el texto.

- **PDF**: Diapositivas en formato `PDF` para su visualización *offline* desde cualquier visor de este tipo de archivos. Por tanto, no requiere conexión a Internet. No contiene las actividades interactivas.

- **Apuntes**: El mismo contenido que las diapositivas anteriores en formato `PDF` pero de manera continua, formando unos apuntes al estilo tradicional. Más apropiado para imprimir en papel.

| Título | HTML | PDF | Apuntes |
| ------ |:----:|:---:|:-------:|
| Conceptos básicos de PHP (I) | [HTML](slides/conceptos-basicos-de-php-i.html){:target="_blank"} | [PDF](pdf/conceptos-basicos-de-php-i.pdf){:target="_blank"} | [Apuntes](apuntes/conceptos-basicos-de-php-i-apuntes.pdf){:target="_blank"}
| Conceptos básicos de PHP II | [HTML](slides/conceptos-basicos-de-php-ii.html){:target="_blank"} | [PDF](pdf/conceptos-basicos-de-php-ii.pdf){:target="_blank"} | [Apuntes](apuntes/conceptos-basicos-de-php-ii-apuntes.pdf){:target="_blank"}
| Interoperabilidad | [HTML](slides/interoperabilidad.html){:target="_blank"} | [PDF](pdf/interoperabilidad.pdf){:target="_blank"} | [Apuntes](apuntes/interoperabilidad-apuntes.pdf){:target="_blank"}
| Introducción a Yii 2 | [HTML](slides/introduccion-a-yii2.html){:target="_blank"} | [PDF](pdf/introduccion-a-yii2.pdf){:target="_blank"} | [Apuntes](apuntes/introduccion-a-yii2-apuntes.pdf){:target="_blank"}
| Estructura a pequeña escala de una aplicación Yii 2 | [HTML](slides/estructura-a-pequena-escala-de-una-aplicacion-yii2.html){:target="_blank"} | [PDF](pdf/estructura-a-pequena-escala-de-una-aplicacion-yii2.pdf){:target="_blank"} | [Apuntes](apuntes/estructura-a-pequena-escala-de-una-aplicacion-yii2-apuntes.pdf){:target="_blank"}
| Estructura a gran escala de una aplicación Yii 2 | [HTML](slides/estructura-a-gran-escala-de-una-aplicacion-yii2.html){:target="_blank"} | [PDF](pdf/estructura-a-gran-escala-de-una-aplicacion-yii2.pdf){:target="_blank"} | [Apuntes](apuntes/estructura-a-gran-escala-de-una-aplicacion-yii2-apuntes.pdf){:target="_blank"}
| Gestión de peticiones en Yii 2 | [HTML](slides/gestion-de-peticiones-en-yii2.html){:target="_blank"} | [PDF](pdf/gestion-de-peticiones-en-yii2.pdf){:target="_blank"} | [Apuntes](apuntes/gestion-de-peticiones-en-yii2-apuntes.pdf){:target="_blank"}
